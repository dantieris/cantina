
import form.LoginForm;
import java.util.Date;

/**
 * Esta classe representa o inicio da aplicação,
 * por onde começa o sistema, no qual inicialmente
 * é instanciado um objeto do formulario de login e 
 * definido como visível.
 */
public class Main {

    public static void main(String[] args) {        

    new LoginForm().setVisible(true);        
    
    }
}
