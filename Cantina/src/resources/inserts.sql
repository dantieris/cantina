INSERT INTO "Accounts" ("balance")
VALUES (0);
INSERT INTO "Accounts" ("balance")
VALUES (0);
INSERT INTO "Accounts" ("balance")
VALUES (0);
INSERT INTO "Accounts" ("balance")
VALUES (0);

INSERT INTO "Users" ("email", "password")
VALUES ('client1@email.com', '1234');
INSERT INTO "Users" ("email", "password")
VALUES ('client2@email.com', '1234');
INSERT INTO "Users" ("email", "password")
VALUES ('client3@email.com', '1234');
INSERT INTO "Users" ("email", "password")
VALUES ('client4@email.com', '1234');
INSERT INTO "Users" ("email", "password")
VALUES ('admin@email.com', '1234');
INSERT INTO "Users" ("email", "password")
VALUES ('employee1@email.com', '1234');
INSERT INTO "Users" ("email", "password")
VALUES ('employee2@email.com', '1234');
INSERT INTO "Users" ("email", "password")
VALUES ('employee3@email.com', '1234');

INSERT INTO "Clients" ("idAccount", "idUser", "clientName", "enrollmentId")
VALUES (1, 1, 'Client 1', '12345');
INSERT INTO "Clients" ("idAccount", "idUser", "clientName", "enrollmentId")
VALUES (2, 2, 'Client 2', '12346');
INSERT INTO "Clients" ("idAccount", "idUser", "clientName", "enrollmentId")
VALUES (3, 3, 'Client 3', '12347');
INSERT INTO "Clients" ("idAccount", "idUser", "clientName", "enrollmentId")
VALUES (4, 4, 'Client 4', '12348');

INSERT INTO "Employees" ("name", "idUser")
VALUES ('Administrator', 5);
INSERT INTO "Employees" ("name", "idUser")
VALUES ('Employee 1', 6);
INSERT INTO "Employees" ("name", "idUser")
VALUES ('Employee 2', 7);
INSERT INTO "Employees" ("name", "idUser")
VALUES ('Employee 3', 8);

INSERT INTO "Products" ("name", "type", "price", "quantity")
VALUES ('Product 1', 'SALGADO', 2.5, 5);
INSERT INTO "Products" ("name", "type", "price", "quantity")
VALUES ('Product 2', 'SALGADO', 4.5, 10);
INSERT INTO "Products" ("name", "type", "price", "quantity")
VALUES ('Product 3', 'DOCE', 6.5, 15);
INSERT INTO "Products" ("name", "type", "price", "quantity")
VALUES ('Product 4', 'BEBIDA', 8.5, 20);
INSERT INTO "Products" ("name", "type", "price", "quantity")
VALUES ('Product 5', 'BEBIDA', 10.5, 25);



INSERT INTO "Sales" ("date", "idEmployee", "idClient") VALUES ('2014-11-26', 1, 1);
INSERT INTO "Sales" ("date", "idEmployee", "idClient") VALUES ('2014-11-26', 2, 1);
INSERT INTO "Sales" ("date", "idEmployee", "idClient") VALUES ('2014-11-26', 1, 2);
INSERT INTO "Sales" ("date", "idEmployee", "idClient") VALUES ('2013-01-11', 2, 2);


INSERT INTO "ItensSales" ("idSale", "idProduct") VALUES (1,1);
INSERT INTO "ItensSales" ("idSale", "idProduct") VALUES (1,2);
INSERT INTO "ItensSales" ("idSale", "idProduct") VALUES (1,3);
INSERT INTO "ItensSales" ("idSale", "idProduct") VALUES (2,2);