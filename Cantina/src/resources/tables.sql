
CREATE TABLE "Accounts"(
"idAccount" INT not null primary key GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
"balance" double default 0
);

CREATE TABLE "Users" (
"idUser" INT not null primary key GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
"email" varchar(255) not null,
"password" varchar(255) not null,
UNIQUE ("email")
);

CREATE TABLE "Clients"(
"idClient" INT not null primary key GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
"idAccount" int,
"clientName" varchar(255) not null,
"idUser" int not null,
"enrollmentId" varchar(255),
FOREIGN KEY ("idAccount") REFERENCES "Accounts"("idAccount"),
FOREIGN KEY ("idUser") REFERENCES "Users"("idUser")
);

CREATE TABLE "Employees"(
"idEmployee" INT not null primary key GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
"name" varchar(255) not null,
"idUser" int not null,
FOREIGN KEY ("idUser") REFERENCES "Users"("idUser")
);

CREATE TABLE "Products"(
"idProduct" INT not null primary key GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
"name" varchar(255) not null,
"type" varchar(255),
"price" double not null,
"quantity" int default 0
);


CREATE TABLE "Sales"(
"idSale" INT not null primary key GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
"date" date,
"idEmployee" int not null,
"idClient" int not null,
FOREIGN KEY ("idEmployee") REFERENCES "Employees"("idEmployee"),
FOREIGN KEY ("idClient") REFERENCES "Clients"("idClient")
);

CREATE TABLE "ItensSales"(
"idItemSale" INT not null primary key GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
"idSale" int not null,
"idProduct" int not null,
FOREIGN KEY ("idProduct") REFERENCES "Products"("idProduct"),
FOREIGN KEY ("idSale") REFERENCES "Sales"("idSale")
);

-- abaixo tabela sales antiga
--CREATE TABLE "Sales"(
--"idSale" INT not null primary key GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
--"date" date,
--"idProduct" int not null,
--"idEmployee" int not null,
--"idClient" int not null,
--FOREIGN KEY ("idProduct") REFERENCES "Products"("idProduct"),
--FOREIGN KEY ("idEmployee") REFERENCES "Employees"("idEmployee"),
--FOREIGN KEY ("idClient") REFERENCES "Clients"("idClient")
--);
