/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author Guilherme
 */
public class NotEnoughFundsException extends Exception {

    /**
     * Contrutor padrao, responsavel por exibir a mensagem padrao ao ser criado
     */
    public NotEnoughFundsException() {
        super("Your account does not have enough funds for this operation.");
    }

    /**
     * Contrutor que possibilita que se gere uma mensagem
     *
     * @param message
     */
    public NotEnoughFundsException(String message) {
        super(message);
    }

}
