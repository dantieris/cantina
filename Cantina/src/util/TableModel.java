/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import model.Product;

/**
 *
 * @author Dantieris
 */
public class TableModel extends AbstractTableModel {

    private List<Product> rows;
    private String[] header;

    public TableModel(String[] header, List rows) {
        this.header = header;
        this.rows = rows;
    }

    public TableModel(String[] header) {
        this.header = header;
        this.rows = new ArrayList<Product>();
    }

    public List<Product> getRows() {
        return rows;
    }

    public String[] getHeader() {
        return header;
    }

    public void setRows(ArrayList rows) {
        this.rows = rows;
    }

    public void setHeader(String[] header) {
        this.header = header;
    }

    @Override
    public int getColumnCount() {
        return header.length;
    }

    @Override
    public int getRowCount() {
        return rows.size();
    }

    @Override
    public String getColumnName(int column) {
        return header[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return (rows.get(rowIndex).getIdProduct());
            case 1:
                return (rows.get(rowIndex).getName());
            case 2:
                return (rows.get(rowIndex).getType());
            case 3:
                return (rows.get(rowIndex).getPrice());
            case 4:
                return (rows.get(rowIndex).getQuantity());
            default:
                return null;
        }
    }
    
}
