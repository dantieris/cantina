/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import java.util.List;
import model.Employee;
import model.EmployeeDAO;
import model.User;
import model.UserDAO;

/**
 * Realiza a interface entre as daos Employee e User.
 */
public class EmployeesFacade implements InterfaceFacade<Employee> {

    private User user;
    private Employee employee;
    private EmployeeDAO employeeDAO;
    private UserDAO userDAO;

    public EmployeesFacade() {
        this.user = null;
        this.employee = null;
        this.employeeDAO = new EmployeeDAO();
        this.userDAO = new UserDAO();
    }

    public Employee getEmployee() {
        return this.employee;
    }

    /**
     * Insere um funcionario da tabela do banco de dados,
     * recebendo-o como parâmetro.
     * @param e Employee funcionario a ser adicionado.
     */
    @Override
    public void insert(Employee e) {
        user = e.getUser();

        userDAO.insert(user);

        e.setUser(userDAO.getUser());

        employeeDAO.insert(e);

        employee = e;
    }

    @Override
    public void delete(Employee e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Employee e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List list() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <e extends Employee> Employee select() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
