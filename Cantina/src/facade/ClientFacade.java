/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import java.util.List;
import model.Account;
import model.AccountDAO;
import model.Client;
import model.ClientDAO;
import model.User;
import model.UserDAO;
import util.AccountNotFoundException;
import util.NotEnoughFundsException;

/**
 * Realiza a interface entre as daos do User, Client e Account.
 */
public class ClientFacade implements InterfaceFacade<Client> {

    private User user;
    private Account account;
    private UserDAO userDAO;
    private AccountDAO accountDAO;
    private ClientDAO clientDAO;
    private Client client;

    public ClientFacade() {
        user = null;
        account = null;
        client = null;
        userDAO = new UserDAO();
        accountDAO = new AccountDAO();
        clientDAO = new ClientDAO();

    }

    public Client getClient() {
        return this.client;
    }

    /**
     * Recebe um client contendo também um user e saldo inicial da conta insere
     * (cria) uma nova cona no banco e entao busca essa nova conta (ja com o id
     * gerado automaticamente pelo banco de dados. faz o mesmo com o user e
     * entao armazena esses objetos atualizados no client finalmente insere um
     * novo client (agora com todas as informacoes de usuario(id) e conta(id)
     * completas.
     *
     * @param e
     */
    public void insert(Client e) {
        user = e.getUser();
        account = e.getAccount();
        accountDAO.insert(account);
        userDAO.insert(user);
        e.setAccount(accountDAO.getAccount());
        e.setUser(userDAO.getUser());
        clientDAO.insert(e);
        client = e;
    }

    /**
     * Busca uma conta a partir do id de um objeto Acccout recebido de um objeto
     * client, caso não exista a conta gera uma exessao
     *
     * @param e
     * @return
     * @throws AccountNotFoundException
     */
    public Account checkAccountBalance(Client e) throws AccountNotFoundException {
        account = accountDAO.getAccountByIdAccount(e.getAccount());
        if (account == null) {
            throw new AccountNotFoundException();
        }

        return account;
    }

    /**
     * Recebe uma conta com apenas o ID, entao busca a conta inteira adiciona o
     * valor a outra conta remove o valor da propria conta. retorna verdadeiro /
     * falso dependendo do sucesso da operacao.
     *
     * @param from
     * @param toReceive
     * @return
     */
    public boolean makeTransfer(Account from, Account toReceive, double valueToTransfer) throws NotEnoughFundsException {
        account = accountDAO.getAccountByIdAccount(from);
        Account accountToReceive = accountDAO.getAccountByIdAccount(toReceive);

        account.debit(valueToTransfer);
        accountToReceive.credit(valueToTransfer);

        accountDAO.update(account);
        accountDAO.update(accountToReceive);

        System.out.println(account.getBalance());
        return true;
    }

    @Override
    public void delete(Client e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Client e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <e extends Client> Client select() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Client> list() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
