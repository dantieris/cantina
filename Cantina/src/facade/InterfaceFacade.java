/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import java.util.List;

/**
 * Representa a interface com os métodos padrão para uma classe facade.
 */
public interface InterfaceFacade<E> {
    
    public void insert(E e);

    public void delete(E e);

    public void update(E e);

    public <e extends E> E select(); 
    
    public List<E> list();
    
}
