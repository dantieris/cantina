/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import java.util.ArrayList;
import java.util.List;
import model.Account;
import model.AccountDAO;
import model.Client;
import model.DAO;
import model.Employee;
import model.Product;
import model.ProductDAO;
import model.Sale;
import model.SaleDAO;
import util.NotEnoughFundsException;

/**
 *
 * @author 180901405
 */
public class SalesFacade implements InterfaceFacade<Sale> {

    private AccountDAO accountDAO;    
    private SaleDAO saleDAO;
    private ProductDAO productDAO;
    public SalesFacade() {
        accountDAO = new AccountDAO();
        saleDAO = new SaleDAO();
        productDAO = new ProductDAO();
    }

    /**
     * busca uma client account pelo id do cliente checa se a conta do cliente
     * tem saldo suficiente e faz o desconto (no objeto) cria um novo registro
     * sale com o id do client e id do employee e para cada item da sale,
     * adiciona um novo registro na itenssale
     *
     * @param e
     */
    public void registerSale(Sale e) throws NotEnoughFundsException {
        Account clientAccount = accountDAO.getAccountByIdAccount(e.getClient().getAccount());
        clientAccount.debit(e.getTotalPrice());
        System.out.println("INFORMACOES DA CONTA: "+clientAccount.getIdAccount() + " - "+clientAccount.getBalance());
        accountDAO.update(clientAccount);
        System.out.println("ATUALIZADO: "+accountDAO.getAccountByIdAccount(accountDAO.getAccount()).getBalance());
        saleDAO.insert(e);
        Sale sale = saleDAO.getSale();//sale COM ID
        for (Product itenSale : sale.getProductList()) {
            saleDAO.insertItenSale(sale, itenSale);
        }
    }
    
    /**
     * Recebe um cliente, busca os itens por id e recebe
     * o id de cada um dos itens(produtos)
     * para cada um busca o produto correspondente no banco
     * gera uma lista com todos os Produtos
     * retorna a lista
     * @param client
     * @return 
     */
    public List<Product> checkReportByClient(Client client){
        List<Product> productList = saleDAO.reportByClient(client);
        for(Product p : productList){
            System.out.println("PRODUCT LIST NO SALESFACADE: "+p);
        }
        List<Product> productListToReturn = new ArrayList<>();
        //productListToReturn.add(new Product(1));     
        System.out.println("RETORNO COMPLETO: "+productDAO.selectProductById(new Product(1)));
        for(Product toSearch : productList){
            productListToReturn.add(productDAO.selectProductById(toSearch));
        }

        return productListToReturn;        
    }
    
        /**
     * Recebe um employee, busca os itens por id e recebe
     * o id de cada um dos itens(produtos)
     * para cada um busca o produto correspondente no banco
     * gera uma lista com todos os Produtos
     * retorna a lista
     * @param employee
     * @return 
     */
    public List<Product> checkReportByEmployee(Employee employee){
        List<Product> productList = saleDAO.reportByEmployee(employee);
        
        for(Product p : productList){
            System.out.println("PRODUCT LIST NO SALESFACADE: "+p);
        }
        List<Product> productListToReturn = new ArrayList<>();
        //productListToReturn.add(new Product(1));     
        System.out.println("RETORNO COMPLETO: "+productDAO.selectProductById(new Product(1)));
        for(Product toSearch : productList){
            productListToReturn.add(productDAO.selectProductById(toSearch));
        }

        return productListToReturn;        
    }

    @Override
    public void insert(Sale e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Sale e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Sale e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <e extends Sale> Sale select() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Sale> list() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
