/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import util.NotEnoughFundsException;

/**
 *
 * @author Guilherme
 */
public class Account {

    private int idAccount;
    private double balance;

    public Account(double valor) {
        this.balance = valor;
    }

    public Account(int idAccount, double valor) {
        this.idAccount = idAccount;
        this.balance = valor;
    }

    public Account(int idAccount) {
        this.idAccount = idAccount;
    }

    public int getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(int idAccount) {
        this.idAccount = idAccount;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double valor) {
        this.balance = valor;
    }

    /**
     * Remove uma certa quantidade (toRemove) do balance da conta, caso a conta
     * nao tenha dinheiro suficiente, gera uma excessao.
     *
     * @param toRemove
     * @throws NotEnoughFundsException
     */
    public void debit(double toRemove) throws NotEnoughFundsException {
        if (this.balance > toRemove) {
            this.balance -= toRemove;
        } else {
            throw new NotEnoughFundsException();
        }
    }

    /**
     * Adiciona um determinado valor para o balance da conta.
     *
     * @param toAdd
     */
    public void credit(double toAdd) {
        this.balance += toAdd;
    }

}
