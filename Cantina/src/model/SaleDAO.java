/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dantieris
 */
public class SaleDAO extends DAO implements InterfaceDAO<Sale> {

    private Sale sale;

    public Sale getSale() {
        return sale;
    }

    public void setSale(Sale sale) {
        this.sale = sale;
    }

    /**
     * insere uma conta na tabela sales, e juntamente na tabela itens sales.
     *
     * @param e
     */
    @Override
    public void insert(Sale e) {
        String sql = "INSERT INTO \"Sales\" (\"date\", \"idEmployee\", \"idClient\") VALUES (?, ?, ?)";
        try {
            this.startConnection(sql);
            System.out.println(sql);
            this.insertQuery.setString(1, e.getDate());
            this.insertQuery.setInt(2, e.getEmployee().getIdEmployee());
            this.insertQuery.setInt(3, e.getClient().getIdClient());
            System.out.println(" - " + e.getDate() + " - " + e.getEmployee().getIdEmployee() + " -" + e.getClient().getIdClient());
            this.insertQuery.executeUpdate();
            ResultSet generatedIndice = insertQuery.getGeneratedKeys();
            generatedIndice.next();//o resultset inicia 
            e.setIdSale(generatedIndice.getInt(1));
            sale = e;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Adiciona um item (Product) a lista de vendas
     * @param sale
     * @param product 
     */
    public void insertItenSale(Sale sale, Product product) {
        String sql = "INSERT INTO \"ItensSales\" (\"idSale\", \"idProduct\") VALUES (?,?)";
        try {
            this.startConnection(sql);
            this.insertQuery.setInt(1, sale.getIdSale());
            this.insertQuery.setInt(2, product.getIdProduct());
            this.insertQuery.executeUpdate();
            ResultSet generatedIndice = insertQuery.getGeneratedKeys();
            generatedIndice.next();//o resultset inicia                         
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(Sale e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Sale e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Sale> list() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Retorna uma lista com uma consulta buscando pelo id cliente.
     *
     * @param e
     * @return
     */
    public List<Product> reportByClient(Client e) {

        String sql = "SELECT * FROM \"ItensSales\" "
                + "INNER JOIN \"Sales\" "
                + "ON \"ItensSales\".\"idSale\" = \"Sales\".\"idSale\" \n"
                + "WHERE \"Sales\".\"idClient\" = ? "
                + "ORDER BY \"Sales\".\"date\"";

        List<Product> productList = new ArrayList<>();

        try {
            System.out.println(sql + e.getIdClient());
            this.startConnection(sql);

            query.setInt(1, e.getIdClient());

            ResultSet data = this.query.executeQuery();
            Product product = null;
            while (data.next()) {
                int id = data.getInt("idProduct");
                System.out.println("ID::::" + id);
                product = new Product(id);
                productList.add(product);

            }
            return productList;
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                this.closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return productList;
    }

    /**
     * gera um relatorio por vendas de um empregado
     *
     * @param e
     * @return
     */
    public List<Product> reportByEmployee(Employee e) {
        String sql = "SELECT * FROM \"ItensSales\" INNER JOIN \"Sales\" ON \"ItensSales\".\"idSale\" = \"Sales\".\"idSale\" \n"
                + "WHERE \"Sales\".\"idEmployee\" = ? ORDER BY \"Sales\".\"date\"";

        List<Product> productList = new ArrayList<>();

        try {
            System.out.println(sql + e.getIdEmployee());
            this.startConnection(sql);

            query.setInt(1, e.getIdEmployee());

            ResultSet data = this.query.executeQuery();
            Product product = null;
            while (data.next()) {
                int id = data.getInt("idProduct");
                System.out.println("ID::::" + id);
                product = new Product(id);
                productList.add(product);

            }
            return productList;
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                this.closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return productList;
    }

}
