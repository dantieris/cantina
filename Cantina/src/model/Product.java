package model;

public class Product {

    private int idProduct;
    private String name;
    private Category type;
    private double price;
    private int quantity;

    public Product(int idProduct, String name, Category type, double price, int quantity) {
        this.setIdProduct(idProduct);
        this.setName(name);
        this.setType(type);
        this.setPrice(price);
        this.setQuantity(quantity);
    }

    public Product(String name, Category type, double price, int quantity) {
        this.name = name;
        this.type = type;
        this.price = price;
        this.quantity = quantity;
    }

    public Product(int idProduct){
        this.idProduct = idProduct;
    }
    public void setIdProduct(int code) {
        this.idProduct = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(Category type) {
        this.type = type;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public String getName() {
        return name;
    }

    public Category getType() {
        return type;
    }

    public double getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }
    
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    public String toString() {
        return "ID: "+idProduct+" - Name: "+this.name+" - Price: "+this.price;
    }
}
