/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 180901405
 */
public class ProductDAO extends DAO implements InterfaceDAO<Product> {

    /**
     * Insere um novo produto na tabela a partir de um objeto do tipo Product
     *
     * @param e
     */
    public void insert(Product e) {
        String sql = "INSERT INTO \"Products\"(\"name\",\"type\", \"price\", \"quantity\") VALUES (?,?,?,?)";
        try {
            this.startConnection(sql);
            this.insertQuery.setString(1, e.getName());
            this.insertQuery.setString(2, e.getType().getDescription());
            this.insertQuery.setDouble(3, e.getPrice());
            this.insertQuery.setInt(4, e.getQuantity());
            this.insertQuery.executeUpdate();
            ResultSet generatedIndice = insertQuery.getGeneratedKeys();
            generatedIndice.next();//o resultset inicia 
            e.setIdProduct(generatedIndice.getInt(1));
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Busca um produto pelo ID
     *
     * @param toSearch
     * @return
     */
    public Product selectProductById(Product toSearch) {
        String sql = "SELECT * FROM \"Products\" WHERE \"idProduct\" = ?";
        Product product = null;
        try {
            startConnection(sql);
            query.setInt(1, toSearch.getIdProduct());
            ResultSet data = query.executeQuery();
                      
            if(data.next()){
                int id = data.getInt("idProduct");
                String name = data.getString("name");
                Category type = null;
                double price = data.getDouble("price");
                int quantity = data.getInt("quantity");

                /*switch (data.getString("type")) {
                    case "SALGADO":
                        type = Category.SALGADO;
                        break;
                    case "DOCE":
                        type = Category.DOCE;
                        break;
                    case "BEBIDA":
                        type = Category.BEBIDA;
                        break;
                    default:
                        type = null;
                }*/

                product = new Product(id, name, type, price, quantity);
                return product;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return product;
    }

    @Override
    public void delete(Product e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Product e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Realiza uma consulta de todos os resultados da tabela dos produtos
     * retornando-os em uma lista.
     *
     * @return
     */
    @Override
    public List<Product> list() {
        String sql = "SELECT * FROM \"Products\"";
        List<Product> listaContatos = new ArrayList<>();
        try {
            this.startConnection(sql);
            ResultSet data = this.query.executeQuery();
            while (data.next()) {
                int id = data.getInt("idProduct");
                String name = data.getString("name");
                Category type = null;
                double price = data.getDouble("price");
                int quantity = data.getInt("quantity");

                switch (data.getString("type")) {
                    case "SALGADO":
                        type = Category.SALGADO;
                        break;
                    case "DOCE":
                        type = Category.DOCE;
                        break;
                    case "BEBIDA":
                        type = Category.BEBIDA;
                        break;
                    default:
                        type = null;
                }

                Product product = new Product(id, name, type, price, quantity);

                listaContatos.add(product);

            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                this.closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return listaContatos;
    }

}
