package model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Sale {

    private int idSale;
    private Client client;
    private Employee employee;
    private ArrayList<Product> productList;
    private double totalPrice;    
    private Date date;

    
    
    
    public Sale(Client client, Employee employee) {
        this.client = client;
        this.employee = employee;
        this.productList = new ArrayList<>();
        this.date = new Date();
    }
    
    public Sale(int idSale, Client client, Employee employee, ArrayList<Product> productList) {
        this.idSale = idSale;
        this.client = client;
        this.employee = employee;
        this.productList = productList;        
    }
    
    public Sale(Client client, Employee employee, ArrayList<Product> productList) {
        this.client = client;
        this.employee = employee;
        this.productList = productList;
        this.date = new Date();        
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    
    
    
    public int getIdSale() {
        return idSale;
    }

    public void setIdSale(int idSale) {
        this.idSale = idSale;
    }

    public ArrayList<Product> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<Product> productList) {
        this.productList = productList;
    }
    
    

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public void addProduct(Product product) {
        this.productList.add(product);
        this.totalPrice += product.getPrice() * product.getQuantity();
    }

    public Product getProduct(int index) {
        return this.productList.get(index);
    }

    @SuppressWarnings("empty-statement")
    public String getDate() {
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
