package model;

public class Employee {

    private int id;
    private String name;
    private User user;

    public Employee(int id, String name, User user) {
        this.user = user;
        this.id = id;
        this.name = name;
    }
    
    public Employee(int id, String name){
        this.id = id;
        this.name = name;
    }
    
    public Employee(String name, User user) {
        this.name = name;
        this.user = user;
    }

    public int getIdEmployee() {
        return id;
    }

    public void setIdEmployee(int id) {
        this.id = id;
    }
    
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    public String toString() {
        return this.getIdEmployee()+" - "+name;
    }
    
}
