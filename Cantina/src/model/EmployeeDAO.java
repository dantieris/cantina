/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 180901405
 */
public class EmployeeDAO extends DAO implements InterfaceDAO<Employee> {

    private Employee employee;

    public Employee getEmployee() {
        return this.employee;
    }

    /**
     * Insere um funcionario na tabela do banco
     *
     * @param e
     */
    @Override
    public void insert(Employee e) {
        String sql = "INSERT INTO \"Employees\" (\"name\", \"idUser\") VALUES (?,?)";
        try {
            this.startConnection(sql);
            this.insertQuery.setString(1, e.getName());
            this.insertQuery.setInt(2, e.getUser().getId());
            this.insertQuery.executeUpdate();
            ResultSet generatedIndice = this.insertQuery.getGeneratedKeys();
            generatedIndice.next();
            e.setIdEmployee(generatedIndice.getInt(1));
            this.employee = e;
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                this.closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        ;
    }

    /**
     * Realiza uma busca na tabela de empregados de acordo com o id do usuario e
     * armazena no campo da classe
     *
     * @param user
     * @return
     */
    public Employee selectById(User user) {
        String sql = "SELECT * FROM \"Employees\" WHERE \"idUser\"=?";
        employee = null;
        try {
            startConnection(sql);
            query.setInt(1, user.getId());
            System.out.println(sql + " - " + user.getId());
            ResultSet resultado = query.executeQuery();
            if (resultado.next()) {
                employee = new Employee(
                        resultado.getInt("idEmployee"),
                        resultado.getString("name"), user);
            } else {
                System.out.println("NAO TROUXE RESULTADO!");
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return employee;
    }

    @Override
    public void delete(Employee e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Employee e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Employee> list() {
        String sql = "SELECT * FROM \"Employees\"";

        List<Employee> employeeList = new ArrayList<>();

        try {
            this.startConnection(sql);

            ResultSet data = this.query.executeQuery();

            while (data.next()) {
                int id = data.getInt("idEmployee");
                String name = data.getString("name");

                Employee employee = new Employee(id, name);

                employeeList.add(employee);
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                this.closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return employeeList;
    }

}
