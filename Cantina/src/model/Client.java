package model;

public class Client {

    private int id;
    private User user;
    private String enrollmentId;
    private Account account;
    private String name;
    
    public Client(int id, String enrollmentId, String name, String email, String password) {
        this.user = new User(email, password);
        this.id = id;
        this.enrollmentId = enrollmentId;
        this.name = name;
    }
    
    public Client(int id, String enrollmentId, String name, User user, Account account){
        this.user = user;
        this.id = id;
        this.enrollmentId = enrollmentId;
        this.name = name;
        this.account = account;
    }
    
    /**
     * Construtor utilizado para o facade
     * @param enrollmentId
     * @param name
     * @param user
     * @param account 
     */
    public Client(String enrollmentId, String name, User user, Account account){
        this.enrollmentId = enrollmentId;
        this.name = name;
        this.user = user;
        this.account = account;
    }
    
     public Client(String enrollmentId, String name, String email, String password) {                
        this.enrollmentId = enrollmentId;
        this.name = name;
        this.user = new User(email, password);
    }
    

    public String getEnrollmentId() {
        return enrollmentId;
    }

    public void setEnrollmentId(String enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    public Account getConta() {
        return account;
    }

    public void setConta(Account account) {
        this.account = account;
    }

    public int getIdClient() {
        return id;
    }

    public void setIdClient(int id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getEmail(){
        return this.user.getEmail();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return id+" - "+name+" - "+enrollmentId;
    }
    
}
