package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Guilherme
 */
public class ClientDAO extends DAO implements InterfaceDAO<Client> {

    private Client client;

    public Client getClient() {
        return this.client;
    }

    /**
     * Recebe um client, recupera as informações dele e insere no banco
     * pega o id gerado pelo banco e armazena no client recebido
     * então adiciona esse cliente completo(com o id gerado pelo banco)
     * na referencia da DAO para que seja possivel utiliza-lo (id) por
     * outras classes    
     * Campos da tabela client: idClient, idAccount, clientName, idUser, enrollmentId
     * @param e
     */
    @Override
    public void insert(Client e) {
        //exemplo de como a query precisa ficar:
        //INSERT INTO "Clients"("name", "email", "enrollmentId") VALUES('Guilherme','guilherme@guilherme' ,'123123')
        String sql = "INSERT INTO \"Clients\"(\"clientName\", \"enrollmentId\", \"idAccount\",\"idUser\") VALUES(?, ?, ?, ?)";
        try {
            this.startConnection(sql);
            this.insertQuery.setString(1, e.getName());            
            this.insertQuery.setString(2, e.getEnrollmentId());
            this.insertQuery.setInt(3, e.getAccount().getIdAccount());
            this.insertQuery.setInt(4, e.getUser().getId());
            this.insertQuery.executeUpdate();
            ResultSet generatedIndice = insertQuery.getGeneratedKeys();
            generatedIndice.next();//o resultset inicia 
            e.setIdClient(generatedIndice.getInt(1));
            this.client = e;
            System.out.println("Client id: " + this.client.getIdClient());
        } catch (SQLException ex) {
            Logger.getLogger(ClientDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ClientDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                this.closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(ClientDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    /**
     * Metodo responsavel por delete um cliente da tabela utiliza o email como
     * parametro unico de identificacao
     *
     * @param e
     */
    @Override
    public void delete(Client e) {
        String sql = "DELETE FROM \"Clients\" WHERE \"email\" = ?";
        try {
            this.startConnection(sql);
            query.setString(1, e.getEmail());
            query.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ClientDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ClientDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                this.closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(ClientDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public Client selectById(User user) {
        String sql = "SELECT * FROM \"Clients\" WHERE \"idUser\"=?";
        client = null;
        try {
            startConnection(sql);
            query.setInt(1, user.getId());
            System.out.println(sql+" - "+user.getId());
            ResultSet resultado = query.executeQuery();            
            if (resultado.next()) {
                client = new Client(
                        resultado.getInt("idClient"),
                        resultado.getString("enrollmentId"),
                        resultado.getString("clientName"), user, new Account(resultado.getInt("idAccount")));                                        
            }                          
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ClientDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(ClientDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return client;
    }
    
    
    /**
     * Metodo responsavel por desativar um cliente no banco, eh uma alternativa
     * a delecao.
     *
     * @param e
     */
    public void inativar(Client e) {
        String sql = "UPDATE Clients SET ativo = 0 WHERE id = ?";
    }

    /**
     * Metodo que 'recupera' um cliente, ativando-o novamente.
     *
     * @param e
     */
    public void ativar(Client e) {
        String sql = "UPDATE Clients SET ativo = 1 WHERE id = ?";
    }

    @Override
    public void update(Client e) {
        String sql = "UPDATE Clients SET nome = ?, email = ?, matricula = ? WHERE id = ?";

    }

    /**
     * Realiza a busca de todos os dados da tabela "Client",
     * e os retorna na forma de uma lista de Client.
     * 
     * O User do client só possui o seu id, 
     * e a Account também.
     * 
     * @return List<Client> lista com todos resutados da tabela "Client".
     */
    @Override
    public List<Client> list() {
        String sql = "SELECT * FROM \"Clients\"";
        
        List<Client> clientList = new ArrayList<>();
        
        try {
            this.startConnection(sql);
            
            ResultSet data = this.query.executeQuery();
            
            while(data.next()) {
                int id = data.getInt("idClient");
                int idAccount = data.getInt("idAccount");
                int idUser = data.getInt("idUser");
                String name = data.getString("clientName");
                String enrollmentId = data.getString("enrollmentId");
                
                Client client = new Client(id, enrollmentId, name, new User(idUser, "", ""), new Account(idAccount));
                
                clientList.add(client);
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                this.closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return clientList;
    }

}
