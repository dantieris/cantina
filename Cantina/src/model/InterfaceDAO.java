package model;

import java.util.List;

/**
 *
 * @author Guilherme Interface gernerica, contendo os metodos que todo DAO
 * precisara
 */
public interface InterfaceDAO<E> {

    public void insert(E e);

    public void delete(E e);

    public void update(E e);

    public List<E> list();

}
