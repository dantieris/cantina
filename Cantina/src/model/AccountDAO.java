/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 180901405
 */
public class AccountDAO extends DAO implements InterfaceDAO<Account> {

    private Account account;
    
    public Account getAccount(){
        return this.account;
    }
    
    /**
     * Salava no banco o saldo inicial de uma conta, recupera o id gerado
     * para essa nova conta e adiciona ao campo da classe para que outras 
     * classes possam utilizar.
     * @param e 
     */
    public void insert(Account e) {
        String sql = "INSERT INTO \"Accounts\" (\"balance\") VALUES (?)";
        try {
            this.startConnection(sql);
            this.insertQuery.setDouble(1, e.getBalance());
            this.insertQuery.executeUpdate();
            ResultSet generatedIndice = this.insertQuery.getGeneratedKeys();
            generatedIndice.next();
            e.setIdAccount(generatedIndice.getInt(1));
            this.account = e;
            System.out.println("account id: " + this.account.getIdAccount());
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Realiza uma consulta no banco persquisando
     * pela id da conta, armazena a conta retornada no campo
     * account.
     * @param account
     * @return 
     */
    public Account getAccountByIdAccount(Account account){
        String sql = "SELECT * FROM \"Accounts\" WHERE \"idAccount\" = ?";        
        try {
            startConnection(sql);
            query.setInt(1, account.getIdAccount());
            ResultSet resultado = query.executeQuery();            
            if(resultado.next())
            {
                account = new Account(
                        resultado.getInt("idAccount"),                        
                        resultado.getDouble("balance"));
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return account;
    }
    
    @Override
    public void delete(Account e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Metodo responsavel por atualizar as informacoes de uma conta
     * @param e 
     */
    public void update(Account e) {
        
        String sql = "UPDATE \"Accounts\"\n" +
                    "SET \"balance\" = ?\n" +
                    "WHERE \"idAccount\" = ?";
        
                try {

            this.startConnection(sql);
            
            this.query.setDouble(1, e.getBalance());
            this.query.setInt(2, e.getIdAccount());
            
            query.executeUpdate();
            account = e;
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public List<Account> list() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Realiza um select para verificar se o id da conta
     * é de uma conta válida.
     * @param account
     * @return 
     */
    public Account authenticate(Account account) {
        
        String sql = "SELECT * "
                + "FROM \"Accounts\""
                + "WHERE \"idAccount\" = ?";

        try {

            this.startConnection(sql);
            this.insertQuery.setInt(1, account.getIdAccount());

            ResultSet data = insertQuery.executeQuery();

            data.next();

            if (data == null) {
                return null;
            } else {
                Account authenticated = new Account(data.getInt("idAccount"),
                                    data.getDouble("balance"));

                return authenticated;
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

}
