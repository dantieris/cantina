/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author idcob117
 */
public class UserDAO extends DAO implements InterfaceDAO<User> {

    private User user;

    public User getUser() {
        return this.user;
    }

    /**
     * Recebe um User, carrega as informações no comando sql,
     * realiza um select para verificar se o email já foi cadastrado,
     * caso não tenha sido insere no banco 
     * pega o id gerado pelo banco e armazena no User recebido
     * então adiciona esse cliente completo(com o id gerado pelo banco)
     * na referencia da DAO para que seja possivel utiliza-lo (id) por
     * outras classes
     * 
     * @param e User para inserir no banco de dados.
     */
    @Override
    public void insert(User e) {
        try {
            
            String sqlCheckEmail = "SELECT * "
                                    + "FROM \"Users\""
                                    + "WHERE \"email\" = ?";
            
            this.startConnection(sqlCheckEmail);
            this.query.setString(1, e.getEmail());

            ResultSet data = query.executeQuery();
            
            if (data.getRow() > 0) {
                JOptionPane.showMessageDialog(null, "This email has been already registered");
            } else {
                String sql = "INSERT INTO \"Users\" (\"email\", \"password\") VALUES (?, ?)";
                
                this.startConnection(sql);
                this.insertQuery.setString(1, e.getEmail());
                this.insertQuery.setString(2, e.getPassword());
                this.insertQuery.executeUpdate();
                ResultSet generatedIndice = this.insertQuery.getGeneratedKeys();
                generatedIndice.next();
                e.setId(generatedIndice.getInt(1));
                this.user = e;
                System.out.println("user id: " + this.user.getId());
            }
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ClientDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(User e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(User e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<User> list() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Realiza uma consulta do usuario no banco,
     * verificando se o id user é de um usário válido.
     * @param e
     * @return 
     */
    public User authenticate(User e) {

        String sql = "SELECT * "
                + "FROM \"Users\""
                + "WHERE \"email\" = ?"
                + "AND \"password\" = ?";

        try {

            this.startConnection(sql);
            this.query.setString(1, e.getEmail());
            this.query.setString(2, e.getPassword());

            ResultSet data = query.executeQuery();

            data.next();

            if (data == null) {
                return null;
            } else {
                User authenticated = new User(data.getInt("idUser"),
                        data.getString("email"),
                        data.getString("password"));

                return authenticated;
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ClientDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

}
