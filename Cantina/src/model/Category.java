/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Dantieris
 */

public enum Category {
    
    SALGADO("SALGADO"),
    DOCE("DOCE"),
    BEBIDA("BEBIDA");
    
    private String description;
    
    private Category(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
}
