/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import util.ConnectionFactory;

/**
 * Classe contendo os campos e metodos implementados que serão utilizados por
 * todos so DAOs
 *
 * @author 180901405
 */
public abstract class DAO {

    protected Connection connection;
    protected PreparedStatement query;
    protected PreparedStatement insertQuery;//retorna o indice ao inserir

    /**
     * Metodo responsavel por 'criar' a conexao com o banco de dados utilizando
     * a classe ConnectionFactory E preparar o comando SQL.
     *
     * @param sql
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    protected void startConnection(String sql) throws SQLException, ClassNotFoundException {
        this.connection = ConnectionFactory.getConnection();
        this.query = this.connection.prepareStatement(sql);
        this.insertQuery = this.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
    }

    /**
     * Metodo responsavel por encerrar o comando SQL E a conexao com o banco.
     *
     * @throws SQLException
     */
    protected void closeConnection() throws SQLException {
        this.query.close();
        this.connection.close();
    }

}
